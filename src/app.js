const express = require('express');
const cors = require('cors');
const userRoutes = require('./userRoutes');
const chessRoutes = require('./chessRoutes');
const gameRoutes = require('./gameRoutes');
const rankingRoutes = require('./rankingRoutes');
const auth = require('./auth');
const brute = require('./brute');
const config = require('./config');

var app = express();

//Enable trust proxy
app.enable('trust proxy');

//Allow CORS on all requests
app.use(cors());

//JSON parse middleware
app.use(express.json());

//User Auth
app.post('/register', brute.slow('register'), userRoutes.register);
app.post('/login', brute.slow('login'), auth.softAuthRoute, userRoutes.login);
app.get('/refreshToken', brute.slow('refreshToken'), auth.authRoute, userRoutes.refresh);
app.get('/authCheck', brute.med('authCheck'), auth.authRoute, userRoutes.authCheck);

//User Info
app.get('/users/:username', brute.med('users-username'), auth.softAuthRoute, userRoutes.getInfo);
app.get('/users', brute.med('g-users'), auth.softAuthRoute, userRoutes.getInfoQuery);
app.post('/users', brute.med('p-users'), auth.softAuthRoute, userRoutes.getInfoQuery);
app.post('/users/:username/update', brute.slow('users-username-update'), auth.authRoute, userRoutes.update);

//Session
app.get('/sessions/:id', brute.fast('sessions-id'), auth.softAuthRoute, chessRoutes.getInfo);
app.get('/sessions', brute.med('g-sessions'), auth.softAuthRoute, chessRoutes.getInfoQuery);
app.post('/sessions', brute.med('p-sessions'), auth.softAuthRoute, chessRoutes.getInfoQuery);
app.post('/sessions/new', brute.med('sessions-new'), auth.authRoute, chessRoutes.new);
app.post('/sessions/update', brute.med('sessions-update'), auth.authRoute, chessRoutes.update);
app.post('/sessions/:id/remove', brute.med('sessions-id-remove'), auth.authRoute, chessRoutes.remove);
app.post('/sessions/:id/addUser', brute.med('sessions-id-addUser'), auth.authRoute, chessRoutes.addUser);
app.post('/sessions/:id/requestJoin', brute.med('sessions-id-requestJoin'), auth.authRoute, chessRoutes.requestJoin);
app.post('/sessions/:id/ready', brute.med('sessions-id-ready'), auth.authRoute, chessRoutes.ready);
app.post('/sessions/:id/unready', brute.med('sessions-id-unready'), auth.authRoute, chessRoutes.unready);
app.post('/sessions/:id/start', brute.med('sessions-id-start'), auth.authRoute, chessRoutes.start);
app.post('/sessions/:id/move', brute.med('sessions-id-move'), auth.authRoute, chessRoutes.move);
app.post('/sessions/:id/undo', brute.med('sessions-id-undo'), auth.authRoute, chessRoutes.undo);
app.post('/sessions/:id/submit', brute.med('sessions-id-submit'), auth.authRoute, chessRoutes.submit);
app.post('/sessions/:id/forfeit', brute.med('sessions-id-forfeit'), auth.authRoute, chessRoutes.forfeit);
app.post('/sessions/:id/draw', brute.med('sessions-id-draw'), auth.authRoute, chessRoutes.draw);

//Game
app.get('/games/:id', brute.med('games-id'), auth.softAuthRoute, gameRoutes.getInfo);
app.get('/games', brute.med('g-games'), auth.softAuthRoute, gameRoutes.getInfoQuery);
app.post('/games', brute.med('p-games'), auth.softAuthRoute, gameRoutes.getInfoQuery);

//Ranking
app.get('/rankings/:id', brute.med('rankings-id'), auth.softAuthRoute, rankingRoutes.getInfo);
app.get('/rankings', brute.med('g-rankings'), auth.softAuthRoute, rankingRoutes.getInfoQuery);
app.post('/rankings', brute.med('p-rankings'), auth.softAuthRoute, rankingRoutes.getInfoQuery);

//Xmpp
app.get('/xmpp', brute.med('xmpp'), auth.authRoute, userRoutes.getXmpp);

//General
app.get('/message', brute.med('message'), (req, res) => {
  res.status(200).send(config.motdString);
});
app.get('/', brute.med('root'), (req, res) => {
  res.status(200).send(config.gatewayString);
});
app.get('/_ah/warmup', (req, res) => { res.status(200).end(); });

module.exports = app;
