const config = require('./config');
var ExpressBrute = require('express-brute');
var MongoStore = require('express-brute-mongo');
var MongoClient = require('mongodb').MongoClient;

var getStore = (label) => {
  var store = new ExpressBrute.MemoryStore();
  if(config.db.type === 'mongodb') {
    store = new MongoStore((ready) => {
      MongoClient.connect(config.db.url, config.db.options ? config.db.options : {}, (err, client) => {
        if(err) {
          throw err;
        }
        ready(client.db().collection('brute-' + label));
      });
    });
  }
  return store
}

var bruteSlow = (label) => {
  return new ExpressBrute(getStore(label), {
    freeRetries: 3,
    minWait: 5000,
    maxWait: 5000
  }).prevent;
};

var bruteMed = (label) => {
  return new ExpressBrute(getStore(label), {
    freeRetries: 2,
    minWait: 1000,
    maxWait: 1000
  }).prevent;
};

var bruteFast = (label) => {
  return new ExpressBrute(getStore(label), {
    freeRetries: 3,
    minWait: 250,
    maxWait: 250
  }).prevent;
};

if(config.enableBrute || typeof config.enableBrute === 'undefined') {
  module.exports = {
    slow: bruteSlow,
    med: bruteMed,
    fast: bruteFast
  };
}
else {
  module.exports = {
    slow: () => (req, res, next) => { next(); },
    med: () => (req, res, next) => { next(); },
    fast: () => (req, res, next) => { next(); }
  };
}
