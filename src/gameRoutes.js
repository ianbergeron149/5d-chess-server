const gameInterface = require('./gameInterface');

const gameTransform = (game) => {
  return {
    id: game.id,
    white: game.white,
    black: game.black,
    variant: game.variant,
    format: game.format,
    ranked: game.ranked,
    startDate: game.startDate,
    endDate: game.endDate,
    actionHistory: game.actionHistory,
    winner: game.winner,
    winCause: game.winCause
  };
};

exports.getInfo = async (req, res) => {
  try {
    var game = (await gameInterface.getGame(req.params.id));
    res.status(200).send(gameTransform(game));
  }
  catch(err) {
    if(!res.headersSent) { res.status(500).send({ error: typeof err === 'string' ? err : err.message }); }
  }
};

exports.getInfoQuery = async (req, res) => {
  try {
    if(typeof req.body !== 'object') {
      req.body = {
        query: {},
        projection: {},
        sort: {},
        limit: 100
      };
    }
    if(typeof req.body.query !== 'object') { req.body.query = {}; }
    if(typeof req.body.projection !== 'object') { req.body.projection = {}; }
    if(typeof req.body.sort !== 'object') { req.body.sort = {}; }
    if(typeof req.body.limit !== 'number') { req.body.limit = 100; }
    var games = (await gameInterface.getGames(req.body.query, req.body.projection, req.body.sort, req.body.limit)).map(e => gameTransform(e));
    res.status(200).send(games);
  }
  catch(err) {
    if(!res.headersSent) { res.status(500).send({ error: typeof err === 'string' ? err : err.message }); }
  }
};
