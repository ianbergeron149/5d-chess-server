const userInterface = require('./userInterface');
const secret = require('./secret');
const initXmpp = require('./xmpp/xmpp');
const room = require('./xmpp/room');
const config = require('./config');
const fs = require('fs');

exports.init = async () => {
  try {
    var secretStr = await secret.getSecret();
    console.log('Secret is \"' + secretStr + '\"');
    var botStr = await secret.getNonbot();
    console.log('Bot Token is \"' + botStr + '\"');
    try {
      var adminPass = await secret.getAdmin();
      await userInterface.registerUser('admin', adminPass, { admin: true });
      console.log('Admin account created, password is \"' + adminPass + '\"');
    }
    catch(err) {
      console.error(err);
    }
    try {
      fs.writeFileSync('secrets', 'Secret: ' + secretStr + '\nBot Token: ' + botStr + '\nAdmin Password: ' + adminPass);
      console.log('Passwords and secrets are saved in secrets file');
    }
    catch(err) {
      console.error(err);
    }
    if(config.xmpp.enable) {
      await initXmpp.init();
      for(var i = 0;i < config.xmpp.defaultRooms.length;i++) {
        await room.addRoom(config.xmpp.defaultRooms[i]);
      }
    }
  }
  catch(err) {
    console.error(err);
  }
};
