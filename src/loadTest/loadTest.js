const { spawn, Pool, Worker } = require('threads');
const os = require('os');
var cpuCount = os.cpus().length - 1;
cpuCount = cpuCount <= 0 ? 1 : cpuCount;

const pool = Pool(() => spawn(new Worker('./gameWorker')));

const run = async () => {
  for(var i = 0;i < cpuCount;i++) {
    pool.queue(async (worker) => {
      await worker.multipleGames('http://localhost:5000', 10);
    });
  }
  await pool.completed();
}

run().catch(console.error);