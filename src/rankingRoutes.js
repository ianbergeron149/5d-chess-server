const rankingInterface = require('./rankingInterface');

const rankTransform = (rank) => {
  return {
    id: rank.id,
    username: rank.username,
    rating: rank.rating,
    variant: rank.variant,
    format: rank.format,
    date: rank.date,
    gameId: rank.gameId
  };
};

exports.getInfo = async (req, res) => {
  try {
    var ranking = (await rankingInterface.getRanking(req.params.id));
    res.status(200).send(rankTransform(ranking));
  }
  catch(err) {
    if(!res.headersSent) { res.status(500).send({ error: typeof err === 'string' ? err : err.message }); }
  }
};

exports.getInfoQuery = async (req, res) => {
  try {
    if(typeof req.body !== 'object') {
      req.body = {
        query: {},
        projection: {},
        sort: {},
        limit: 100
      };
    }
    if(typeof req.body.query !== 'object') { req.body.query = {}; }
    if(typeof req.body.projection !== 'object') { req.body.projection = {}; }
    if(typeof req.body.sort !== 'object') { req.body.sort = {}; }
    if(typeof req.body.limit !== 'number') { req.body.limit = 100; }
    var rankings = (await rankingInterface.getRankings(req.body.query, req.body.projection, req.body.sort, req.body.limit)).map(e => rankTransform(e));
    res.status(200).send(rankings);
  }
  catch(err) {
    if(!res.headersSent) { res.status(500).send({ error: typeof err === 'string' ? err : err.message }); }
  }
};
