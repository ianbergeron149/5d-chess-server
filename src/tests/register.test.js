const app = require('../app');
const supertest = require('supertest');
const request = supertest(app);

test('Register and login multiple users', async () => {
  var res1 = await request.post('/register').send({
    username: 'testuser1',
    password: 'testuser1'
  });
  expect(res1.status).toBe(200);
  expect(typeof res1.text).toBe('string');

  var res2 = await request.post('/register').send({
    username: 'testuser2',
    password: 'testuser2',
    bio: 'Test biography'
  });
  expect(res2.status).toBe(200);
  expect(typeof res2.text).toBe('string');

  var res3 = await request.post('/register').send({
    username: 'testuser3',
    password: 'testuser3',
    bio: 'Test biography',
    fullname: 'Test User 3',
    country: 'USA'
  });
  expect(res3.status).toBe(200);
  expect(typeof res3.text).toBe('string');

  var res1 = await request.post('/login').send({
    username: 'testuser1',
    password: 'testuser1'
  });
  expect(res1.status).toBe(200);
  expect(typeof res1.text).toBe('string');

  var res2 = await request.post('/login').send({
    username: 'testuser2',
    password: 'testuser2'
  });
  expect(res2.status).toBe(200);
  expect(typeof res2.text).toBe('string');

  var res3 = await request.post('/login').send({
    username: 'testuser3',
    password: 'testuser3'
  });
  expect(res3.status).toBe(200);
  expect(typeof res3.text).toBe('string');
});

test('Register and login invalid users', async () => {
  var res4 = await request.post('/register').send({
    username: 'testuser4',
    password: 'testuser4'
  });
  expect(res4.status).toBe(200);
  expect(typeof res4.text).toBe('string');

  var res5 = await request.post('/register').send({
    username: 'testuser4',
    password: 'testuser4'
  });
  expect(res5.status).toBe(403);
  expect(res5.body).toStrictEqual({
    error: 'Username already exists!'
  });

  var res6 = await request.post('/register').send({
    username: 'testuser5',
    password: 'testuser5',
    country: 'US'
  });
  expect(res6.status).toBe(403);
  expect(res6.body).toStrictEqual({
    error: 'Country field is not ISO 3166-1 Alpha-3 compliant! (List here: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3#Officially_assigned_code_elements)'
  });

  var res4 = await request.post('/login').send({
    username: 'user1',
    password: 'wrongpassword1'
  });
  expect(res4.status).toBe(403);
  expect(res4.body).toStrictEqual({
    error: 'Username or Password do not match!'
  });

  var res5 = await request.post('/login').send({
    username: 'user2',
    password: 'wrongpassword2'
  });
  expect(res5.status).toBe(403);
  expect(res5.body).toStrictEqual({
    error: 'Username or Password do not match!'
  });
});
