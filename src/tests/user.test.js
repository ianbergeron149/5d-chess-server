const app = require('../app');
const supertest = require('supertest');
const request = supertest(app);

test('Get user information', async () => {
  var res1 = await request.get('/users/user1');
  expect(res1.status).toBe(200);
  expect(res1.body).toMatchObject({
    username: 'user1',
    bio: 'Test biography',
    fullname: 'User 1',
    country: 'USA'
  });

  var res2 = await request.get('/users/user2');
  expect(res2.status).toBe(200);
  expect(res2.body).toMatchObject({
    username: 'user2',
    bio: 'Test biography',
    fullname: 'User 2',
    country: 'USA'
  });
});

test('Try to get missing user information', async () => {
  var res1 = await request.get('/users/user3');
  expect(res1.status).toBe(500);
  expect(res1.body).toStrictEqual({
    error: 'User not found!'
  });
});
