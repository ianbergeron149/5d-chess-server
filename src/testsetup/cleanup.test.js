const fs = require('fs');

test('Dummy test', () => {
  expect(1).toBe(1);
});

afterAll(() => {
  try {
    fs.renameSync('./tmpConfig.json', './config.json');
  }
  catch(err) {}
});
