const fs = require('fs');

beforeAll(async () => {
  try {
    fs.rmdirSync('./db', { recursive: true });
  }
  catch(err) {}
  try {
    fs.renameSync('./config.json', './tmpConfig.json');
  }
  catch(err) {}
  const init = require('../init');
  try {
    await init.init();
  }
  catch(err) {}
  const app = require('../app');
  const supertest = require('supertest');
  const request = supertest(app);
  await request.post('/register').send({
    username: 'user1',
    password: 'user1',
    bio: 'Test biography',
    fullname: 'User 1',
    country: 'USA'
  });
  await request.post('/register').send({
    username: 'user2',
    password: 'user2',
    bio: 'Test biography',
    fullname: 'User 2',
    country: 'USA'
  });
}, 10000);

test('Dummy test', () => {
  expect(1).toBe(1);
});
