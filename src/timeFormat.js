exports.toFormat = (timed) => {
  if(timed === null) {
    return 'untimed';
  }
  else {
    var res = '';
    res += Math.floor(timed.startingDuration / 60) + ':' + Math.floor(timed.startingDuration % 60).toFixed().padStart(2, '0');
    if(timed.perActionFlatIncrement > 0) { res += ';incf' + Math.floor(timed.perActionFlatIncrement); }
    if(timed.perActionTimelineIncrement > 0) { res += ';inct' + Math.floor(timed.perActionTimelineIncrement); }
    if(
      timed.startingDuration === 5 * 60 &&
      timed.perActionFlatIncrement === 0 &&
      timed.perActionTimelineIncrement === 0
    ) {
      res = 'bullet';
    }
    if(
      timed.startingDuration === 10 * 60 &&
      timed.perActionFlatIncrement === 0 &&
      timed.perActionTimelineIncrement === 5
    ) {
      res = 'blitz';
    }
    if(
      timed.startingDuration === 20 * 60 &&
      timed.perActionFlatIncrement === 0 &&
      timed.perActionTimelineIncrement === 10
    ) {
      res = 'rapid';
    }
    if(
      timed.startingDuration === 40 * 60 &&
      timed.perActionFlatIncrement === 0 &&
      timed.perActionTimelineIncrement === 20
    ) {
      res = 'standard';
    }
    if(
      timed.startingDuration === 80 * 60 &&
      timed.perActionFlatIncrement === 0 &&
      timed.perActionTimelineIncrement === 40
    ) {
      res = 'tournament';
    }
    return res;
  }
};

exports.toTimed = (format) => {
  var timed = null;
  if(format === 'bullet') {
    timed = {
      whiteDurationLeft: 0,
      blackDurationLeft: 0,
      startingDuration: 5 * 60,
      perActionFlatIncrement: 0,
      perActionTimelineIncrement: 0,
      lastUpdate: 0
    };
  }
  else if(format === 'blitz') {
    timed = {
      whiteDurationLeft: 0,
      blackDurationLeft: 0,
      startingDuration: 10 * 60,
      perActionFlatIncrement: 0,
      perActionTimelineIncrement: 5,
      lastUpdate: 0
    };
  }
  else if(format === 'rapid') {
    timed = {
      whiteDurationLeft: 0,
      blackDurationLeft: 0,
      startingDuration: 20 * 60,
      perActionFlatIncrement: 0,
      perActionTimelineIncrement: 10,
      lastUpdate: 0
    };
  }
  else if(format === 'standard') {
    timed = {
      whiteDurationLeft: 0,
      blackDurationLeft: 0,
      startingDuration: 40 * 60,
      perActionFlatIncrement: 0,
      perActionTimelineIncrement: 20,
      lastUpdate: 0
    };
  }
  else if(format === 'tournament') {
    timed = {
      whiteDurationLeft: 0,
      blackDurationLeft: 0,
      startingDuration: 80 * 60,
      perActionFlatIncrement: 0,
      perActionTimelineIncrement: 40,
      lastUpdate: 0
    };
  }
  return timed;
}