const config = require('./config');
var collections = require('./db').init();
const bcrypt = require('bcrypt');
const { v4: uuidv4 } = require('uuid');
const xmppUser = require('./xmpp/user');
const xmppAvatar = require('./xmpp/avatar');
const Jimp = require('jimp');
const jdenticon = require('jdenticon');

exports.registerUser = async (username, password, additionalInfo = {}) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser === null) {
    var newUser = {
      avatar: '',
      username: username,
      passwordHash: (await bcrypt.hash(password, config.saltRounds ? config.saltRounds : 10)),
      fullname: '',
      bio: '',
      country: '',
      bot: true,
      joinDate: Date.now(),
      admin: false,
      xmppPass: '',
    };
    if(additionalInfo.fullname) { newUser.fullname = additionalInfo.fullname; }
    if(additionalInfo.bio) { newUser.bio = additionalInfo.bio; }
    if(additionalInfo.country) { newUser.country = additionalInfo.country; }
    if(typeof additionalInfo.bot !== 'undefined') { newUser.bot = additionalInfo.bot; }
    if(additionalInfo.admin) { newUser.admin = additionalInfo.admin; }
    var ret = (await collections.users.insert(newUser));
    await this.setXmppPass(username, ret);
    if(typeof additionalInfo.avatar !== 'undefined') {
      await this.setAvatar(username, additionalInfo.avatar, ret);
    }
    else {
      await this.setRandomAvatar(username, ret);
    }
    console.log('New user registered:');
    console.table(newUser);
    return (await collections.users.findOne({ username: username }));
  }
  else {
    throw 'Username already exists!';
  }
};

exports.authUser = async (username, password) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    await this.setXmppPass(username, existingUser);
    return (await bcrypt.compare(password, existingUser.passwordHash));
  }
  else {
    throw 'User not found!';
  }
};

exports.updateUser = async (username, additionalInfo = {}) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    if(additionalInfo.fullname) { existingUser.fullname = additionalInfo.fullname; }
    if(additionalInfo.bio) { existingUser.bio = additionalInfo.bio; }
    if(additionalInfo.country) { existingUser.country = additionalInfo.country; }
    if(typeof additionalInfo.avatar !== 'undefined') {
      await this.setAvatar(username, additionalInfo.avatar, existingUser);
    }
    else {
      await this.setRandomAvatar(username, existingUser);
    }
    delete existingUser._id;
    await collections.users.update({ username: username }, { $set: existingUser });
    return existingUser;
  }
  else {
    throw 'User not found!';
  }
};

exports.updateLastAuth = async (username) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    existingUser.lastAuth = Date.now();
    delete existingUser._id;
    await collections.users.update({ username: username }, { $set: existingUser });
    return existingUser;
  }
  else {
    throw 'User not found!';
  }
};

exports.getUser = async (username) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    return existingUser;
  }
  else {
    throw 'User not found!';
  }
};

exports.getUsers = async (query = {}, projection = {}, sort = {}, limit = 100) => {
  var res = (await (collections.users.findAsCursor(query, projection).sort(sort).limit(limit)));
  if(!Array.isArray(res)) {
    res = await res.toArray();
  }
  return res;
};

exports.setXmppPass = async (username, existingUser = null) => {
  if(existingUser === null) {
    existingUser = await collections.users.findOne({ username: username });
  }
  //Create xmpp user if not already created
  if(config.xmpp.enable) {
    if(typeof existingUser.xmppPass !== 'string' || (typeof existingUser.xmppPass === 'string' && existingUser.xmppPass.length <= 0)) {
      try {
        var xmppPass = uuidv4();
        await xmppUser.upsertUser(username, xmppPass);
        existingUser.xmppPass = xmppPass;
        await collections.users.update({ username: username }, { $set: existingUser });
      }
      catch(err) {
        console.log('Error upserting xmpp user');
        console.error(err);
      }
    }
  }
}

exports.setRandomAvatar = async (username, existingUser = null) => {
  if(existingUser === null) {
    existingUser = await collections.users.findOne({ username: username });
  }
  //Add random avatar to user if needed
  if(typeof existingUser.avatar !== 'string' || (typeof existingUser.avatar === 'string' && existingUser.avatar.length <= 0)) {
    try {
      var image = (await Jimp.read(jdenticon.toPng(username, 128)));
      image.rgba(false);
      image.background(0xffffffff);
      image.rgba(true);
      image.filterType(Jimp.PNG_FILTER_AUTO);
      image.deflateLevel(9);
      var dataUrl = (await image.getBase64Async(Jimp.MIME_PNG));
      xmppAvatar.upsertAvatar(username, dataUrl, existingUser);
      existingUser.avatar = dataUrl;
      await collections.users.update({ username: username }, { $set: existingUser });
    }
    catch(err) {
      console.log('Error adding avatar');
      console.error(err);
    }
  }
}

exports.setAvatar = async (username, data, existingUser = null) => {
  if(existingUser === null) {
    existingUser = await collections.users.findOne({ username: username });
  }
  //Add avatar to user
  try {
    var image = (await Jimp.read(data));
    image.cover(128, 128, Jimp.HORIZONTAL_ALIGN_CENTER | Jimp.VERTICAL_ALIGN_MIDDLE);
    image.rgba(false);
    image.background(0xffffffff);
    image.rgba(true);
    image.filterType(Jimp.PNG_FILTER_AUTO);
    image.deflateLevel(9);
    var dataUrl = (await image.getBase64Async(Jimp.MIME_PNG));
    xmppAvatar(username, dataUrl, existingUser);
    existingUser.avatar = dataUrl;
    await collections.users.update({ username: username }, { $set: existingUser });
  }
  catch(err) {
    console.log('Error adding avatar');
    console.error(err);
  }
}