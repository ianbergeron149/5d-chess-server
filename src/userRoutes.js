const userInterface = require('./userInterface');
const auth = require('./auth');
const config = require('./config');
const secret = require('./secret');
const countries = require('i18n-iso-countries');
const profanityFilter = require('leo-profanity');

const userTransform = (user) => {
  return {
    avatar: user.avatar,
    username: user.username,
    fullname: user.fullname,
    bio: user.bio,
    country: user.country,
    bot: user.bot,
    joinDate: user.joinDate,
    lastAuth: user.lastAuth ? user.lastAuth : 0,
    admin: user.admin ? user.admin : false,
  };
};

exports.register = async (req, res) => {
  var data = req.body;
  var additionalInfo = {};
  var valid = true;
  var error = '';
  if(typeof data.username !== 'string') {
    error = 'Username is not string!';
    valid = false;
  }
  else {
    data.username = data.username.toLowerCase();
    var regex = /^[\w\-]+$/;
    var match = data.username.match(regex);
    if(match === null || match[0] !== data.username) {
      error = 'Username is invalid! Valid characters are a-z, 0-9, _ and - characters.';
      valid = false;
    }
    else if(profanityFilter.check(data.username)) {
      error = 'Username contains profanity!';
      valid = false;
    }
    else if(config.usernameBlacklist.includes(data.username)) {
      error = 'Username is blacklisted. Pick another username.';
      valid = false;
    }
  }
  if(valid && typeof data.password !== 'string') {
    error = 'Password is not string!';
    valid = false;
  }
  if(valid && typeof data.bio !== 'undefined') {
    if(typeof data.bio !== 'string') {
      error = 'Bio field is not string!';
      valid = false;
    }
    else if(profanityFilter.check(data.bio)) {
      error = 'Bio field contains profanity!';
      valid = false;
    }
    else {
      additionalInfo.bio = data.bio.substr(0, 500);
    }
  }
  if(valid && typeof data.fullname !== 'undefined') {
    if(typeof data.fullname !== 'string') {
      error = 'Fullname field is not string!';
      valid = false;
    }
    else if(profanityFilter.check(data.fullname)) {
      error = 'Fullname field contains profanity!';
      valid = false;
    }
    else {
      additionalInfo.fullname = data.fullname.substr(0, 100);
    }
  }
  if(valid && typeof data.country !== 'undefined') {
    if(typeof data.country !== 'string') {
      error = 'Country field is not string!';
      valid = false;
    }
    else {
      if(typeof countries.getAlpha3Codes()[data.country] !== 'undefined') {
        additionalInfo.country = data.country;
      }
      else {
        error = 'Country field is not ISO 3166-1 Alpha-3 compliant! (List here: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3#Officially_assigned_code_elements)';
        valid = false;
      }
    }
  }
  if(valid && typeof data.avatar !== 'undefined') {
    if(typeof data.avatar !== 'string') {
      error = 'Avatar field is not string!';
      valid = false;
    }
    else {
      additionalInfo.avatar = data.avatar;
    }
  }
  if(valid && typeof data.token === 'string') {
    if(data.token === (await secret.getNonbot())) {
       additionalInfo.bot = false;
    }
  }
  if(valid) {
    try {
      await userInterface.registerUser(data.username.substr(0, 100), data.password.substr(0, 100), additionalInfo);
      var token = (await auth.tokenSign(data.username.substr(0, 100)));
      res.status(200).send(token);
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.login = async (req, res) => {
  var data = req.body;
  var additionalInfo = {};
  var valid = true;
  var error = '';
  if(typeof data.username !== 'string') {
    error = 'Username is not string!';
    valid = false;
  }
  else {
    var regex = /^[\w\-]+$/;
    var match = data.username.match(regex);
    if(match === null || match[0] !== data.username) {
      error = 'Username is invalid! Valid characters are A-Z, a-z, 0-9, _ and - characters.';
      valid = false;
    }
  }
  if(valid && typeof data.password !== 'string') {
    error = 'Password is not string!';
    valid = false;
  }
  if(valid) {
    try {
      var isAuth = await userInterface.authUser(data.username, data.password);
      if(isAuth) {
        var token = (await auth.tokenSign(data.username));
        res.status(200).send(token);
      }
      else {
        if(!res.headersSent) { res.status(403).send({ error: 'Username or Password do not match!' }); }
      }
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.refresh = async (req, res) => {
  if(req.username) {
    try {
      var token = (await auth.tokenSign(req.username));
      res.status(200).send(token);
    }
    catch(err) {
      res.status(500).send({ error: err });
    }
  }
};

exports.authCheck = async (req, res) => {
  if(req.username) {
    try {
      res.status(200).end();
    }
    catch(err) {
      res.status(500).send({ error: err });
    }
  }
};

exports.update = async (req, res) => {
  if(req.username === req.params.username || (await userInterface.getUser(req.username)).admin) {
    var data = req.body;
    var additionalInfo = {};
    var valid = true;
    var error = '';
    if(valid && typeof data.bio !== 'undefined') {
      if(typeof data.bio !== 'string') {
        error = 'Bio field is not string!';
        valid = false;
      }
      else if(profanityFilter.check(data.bio)) {
        error = 'Bio field contains profanity!';
        valid = false;
      }
      else {
        additionalInfo.bio = data.bio.substr(0, 500);
      }
    }
    if(valid && typeof data.fullname !== 'undefined') {
      if(typeof data.fullname !== 'string') {
        error = 'Fullname field is not string!';
        valid = false;
      }
      else if(profanityFilter.check(data.fullname)) {
        error = 'Fullname field contains profanity!';
        valid = false;
      }
      else {
        additionalInfo.fullname = data.fullname.substr(0, 100);
      }
    }
    if(valid && typeof data.country !== 'undefined') {
      if(typeof data.country !== 'string') {
        error = 'Country field is not string!';
        valid = false;
      }
      else {
        if(typeof countries.getAlpha3Codes()[data.country] !== 'undefined') {
          additionalInfo.country = data.country;
        }
        else {
          error = 'Country field is not ISO 3166-1 Alpha-3 compliant! (List here: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3#Officially_assigned_code_elements)';
          valid = false;
        }
      }
    }
    if(valid && typeof data.avatar !== 'undefined') {
      if(typeof data.avatar !== 'string') {
        error = 'Avatar field is not string!';
        valid = false;
      }
      else {
        additionalInfo.avatar = data.avatar;
      }
    }
    if(valid) {
      try {
        var user = await userInterface.updateUser(req.username, additionalInfo);
        res.status(200).send(userTransform(user));
      }
      catch(err) {
        error = typeof err === 'string' ? err : err.message;
        valid = false;
      }
    }
    if(!valid) {
      if(!res.headersSent) { res.status(403).send({ error: error }); }
    }
  }
  else {
    res.status(403).send({ error: 'Requested user is not same as requesting user!' });
  }
};

exports.getInfo = async (req, res) => {
  try {
    var user = (await userInterface.getUser(req.params.username));
    res.status(200).send(userTransform(user));
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : err.message });
  }
};

exports.getInfoQuery = async (req, res) => {
  try {
    if(typeof req.body !== 'object') {
      req.body = {
        query: {},
        projection: {},
        sort: {},
        limit: 100
      };
    }
    if(typeof req.body.query !== 'object') { req.body.query = {}; }
    if(typeof req.body.projection !== 'object') { req.body.projection = {}; }
    if(typeof req.body.sort !== 'object') { req.body.sort = {}; }
    if(typeof req.body.limit !== 'number') { req.body.limit = 100; }
    var users = (await userInterface.getUsers(req.body.query, req.body.projection, req.body.sort, req.body.limit)).map(e => userTransform(e));
    res.status(200).send(users);
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : err.message });
  }
};

exports.getXmpp = async (req, res) => {
  try {
    var user = (await userInterface.getUser(req.username));
    if(!config.xmpp.enable || typeof user.xmppPass !== 'string') {
      res.status(200).send({});
    }
    else {
      res.status(200).send({
        domain: config.xmpp.domain,
        muc: config.xmpp.muc,
        username: `${user.username}@${config.xmpp.domain}`,
        password: user.xmppPass,
        defaultRooms: config.xmpp.defaultRooms.map(e => `${e}@${config.xmpp.muc}`)
      });
    }
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : err.message });
  }
};
