var repl = require('repl');
const { client, xml, jid } = require("@xmpp/client");
var initXmpp = require('./xmpp');

(async () => {
  console.log('Initializing XMPP Connection');
  var xmpp = (await initXmpp.init(true));
  xmpp.on('stanza',(s) => {
    //console.log(s);
    //console.log(s.toString());
    r.context.result = s;
  });
  var r = repl.start('node> ');
  r.context.xmpp = xmpp;
  r.context.client = client;
  r.context.xml = xml;
  r.context.jid = jid;
  r.context.user = require('./user');
  r.context.room = require('./room');
})();

//var message = xml('message', { type: "chat", to: 'alexbay218@xmpp.chessin5d.net' }, xml('body', null, 'hello world'));
//console.log(message.toString());
//xmpp.send(message);

//var message = xml('iq', { id: ('' + Date.now()), type: "set", to: 'xmpp.chessin5d.net' }, xml('command', { xmlns: 'http://jabber.org/protocol/commands', action: 'execute', node: 'http://jabber.org/protocol/admin#add-user'}));
//var message = xml('iq', { id: ('' + Date.now()), type: "set", to: 'xmpp.chessin5d.net' }, xml('command', { xmlns: 'http://jabber.org/protocol/commands', action: 'execute'}));
