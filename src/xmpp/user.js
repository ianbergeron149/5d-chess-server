const config = require('../config');
const { xml } = require("@xmpp/client");
const initXmpp = require('./xmpp');

exports.upsertUser = async (username, password) => {
  if(!config.xmpp.enable) { return null; }
  if(config.usernameBlacklist.includes(username)) { return null; }
  var xmpp = (await initXmpp.init());
  console.log( `Upserting xmpp user: ${username}`);

  await this.addUser(username, password);
  await this.changeUser(username, password);
}

exports.addUser = async (username, password) => {
  if(!config.xmpp.enable) { return null; }
  if(config.usernameBlacklist.includes(username)) { return null; }
  var xmpp = (await initXmpp.init());
  
  //Initiate add user request
  var sessionId = await (new Promise((resolve, reject) => {
    var requestId = `${Date.now()}`;
    var requestMessage = xml('iq', 
      {
        id: requestId,
        to: config.xmpp.domain,
        type: 'set'
      }, 
      xml('command', 
        {
          xmlns: 'http://jabber.org/protocol/commands',
          action: 'execute',
          node: 'http://jabber.org/protocol/admin#add-user'
        }
      )
    );
    xmpp.on('stanza', (result) => {
      if(result.attrs.id === requestId) {
        if(result.attrs.type === 'result') {
          xmpp.removeListener('stanza', arguments.callee);
          resolve(result.children[0].attrs.sessionid);
        }
        else {
          console.error(result);
          reject(new Error('XMPP Server reports error'));
        }
      }
    });
    xmpp.send(requestMessage);
  }));

  //Fill user registration form
  await (new Promise((resolve, reject) => {
    var requestId = `${Date.now()}`;
    var requestMessage = xml('iq', 
      {
        id: requestId,
        to: config.xmpp.domain,
        type: 'set'
      }, 
      xml('command', 
        {
          xmlns: 'http://jabber.org/protocol/commands',
          node: 'http://jabber.org/protocol/admin#add-user',
          sessionid: sessionId
        },
        xml('x',
          {
            xmlns: 'jabber:x:data',
            type: 'submit'
          },
          xml('field', { type: 'hidden', var: 'FORM_TYPE' }, xml('value', null, 'http://jabber.org/protocol/admin')),
          xml('field', { var: 'accountjid' },
            xml('value', null, `${username}@${config.xmpp.domain}`)
          ),
          xml('field', { var: 'password' },
            xml('value', null, password)
          ),
          xml('field', { var: 'password-verify' },
            xml('value', null, password)
          )
        )
      )
    );
    xmpp.on('stanza', (result) => {
      if(result.attrs.id === requestId) {
        if(
          result.attrs.type === 'result' && 
          result.children[0].attrs.status === 'completed'
        ) {
          xmpp.removeListener('stanza', arguments.callee);
          resolve();
        }
        else {
          console.error(result);
          reject(new Error('XMPP Server reports error'));
        }
      }
    });
    xmpp.send(requestMessage);
  }));
}

exports.changeUser = async (username, password) => {
  if(!config.xmpp.enable) { return null; }
  if(config.usernameBlacklist.includes(username)) { return null; }
  var xmpp = (await initXmpp.init());
  
  //Initiate user password change request
  var sessionId = await (new Promise((resolve, reject) => {
    var requestId = `${Date.now()}`;
    var requestMessage = xml('iq', 
      {
        id: requestId,
        to: config.xmpp.domain,
        type: 'set'
      }, 
      xml('command', 
        {
          xmlns: 'http://jabber.org/protocol/commands',
          action: 'execute',
          node: 'http://jabber.org/protocol/admin#change-user-password'
        }
      )
    );
    xmpp.on('stanza', (result) => {
      if(result.attrs.id === requestId) {
        if(result.attrs.type === 'result') {
          xmpp.removeListener('stanza', arguments.callee);
          resolve(result.children[0].attrs.sessionid);
        }
        else {
          console.error(result);
          reject(new Error('XMPP Server reports error'));
        }
      }
    });
    xmpp.send(requestMessage);
  }));

  //Fill user password change form
  await (new Promise((resolve, reject) => {
    var requestId = `${Date.now()}`;
    var requestMessage = xml('iq', 
      {
        id: requestId,
        to: config.xmpp.domain,
        type: 'set'
      }, 
      xml('command', 
        {
          xmlns: 'http://jabber.org/protocol/commands',
          node: 'http://jabber.org/protocol/admin#change-user-password',
          sessionid: sessionId
        },
        xml('x',
          {
            xmlns: 'jabber:x:data',
            type: 'submit'
          },
          xml('field', { type: 'hidden', var: 'FORM_TYPE' }, xml('value', null, 'http://jabber.org/protocol/admin')),
          xml('field', { var: 'accountjid' },
            xml('value', null, `${username}@${config.xmpp.domain}`)
          ),
          xml('field', { var: 'password' },
            xml('value', null, password)
          )
        )
      )
    );
    xmpp.on('stanza', (result) => {
      if(result.attrs.id === requestId) {
        if(
          result.attrs.type === 'result' && 
          result.children[0].attrs.status === 'completed'
        ) {
          xmpp.removeListener('stanza', arguments.callee);
          resolve();
        }
        else {
          console.error(result);
          reject(new Error('XMPP Server reports error'));
        }
      }
    });
    xmpp.send(requestMessage);
  }));
}

exports.deleteUser = async (username) => {
  if(!config.xmpp.enable) { return null; }
  if(config.usernameBlacklist.includes(username)) { return null; }
  var xmpp = (await initXmpp.init());
  
  //Initiate delete user request
  var sessionId = await (new Promise((resolve, reject) => {
    var requestId = `${Date.now()}`;
    var requestMessage = xml('iq', 
      {
        id: requestId,
        to: config.xmpp.domain,
        type: 'set'
      }, 
      xml('command', 
        {
          xmlns: 'http://jabber.org/protocol/commands',
          action: 'execute',
          node: 'http://jabber.org/protocol/admin#delete-user'
        }
      )
    );
    xmpp.on('stanza', (result) => {
      if(result.attrs.id === requestId) {
        if(result.attrs.type === 'result') {
          xmpp.removeListener('stanza', arguments.callee);
          resolve(result.children[0].attrs.sessionid);
        }
        else {
          console.error(result);
          reject(new Error('XMPP Server reports error'));
        }
      }
    });
    xmpp.send(requestMessage);
  }));

  //Fill user delete form
  await (new Promise((resolve, reject) => {
    var requestId = `${Date.now()}`;
    var requestMessage = xml('iq', 
      {
        id: requestId,
        to: config.xmpp.domain,
        type: 'set'
      }, 
      xml('command', 
        {
          xmlns: 'http://jabber.org/protocol/commands',
          node: 'http://jabber.org/protocol/admin#delete-user',
          sessionid: sessionId
        },
        xml('x',
          {
            xmlns: 'jabber:x:data',
            type: 'submit'
          },
          xml('field', { type: 'hidden', var: 'FORM_TYPE' }, xml('value', null, 'http://jabber.org/protocol/admin')),
          xml('field', { var: 'accountjids' },
            xml('value', null, `${username}@${config.xmpp.domain}`)
          )
        )
      )
    );
    xmpp.on('stanza', (result) => {
      if(result.attrs.id === requestId) {
        if(
          result.attrs.type === 'result' && 
          result.children[0].attrs.status === 'completed'
        ) {
          xmpp.removeListener('stanza', arguments.callee);
          resolve();
        }
        else {
          console.error(result);
          reject(new Error('XMPP Server reports error'));
        }
      }
    });
    xmpp.send(requestMessage);
  }));
}