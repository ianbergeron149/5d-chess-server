const config = require('../config');
const { client, xml } = require("@xmpp/client");
const xmppDebug = require("@xmpp/debug");

var xmpp = new client({
  service: config.xmpp.service,
  domain: config.xmpp.domain,
  username: config.xmpp.username,
  password: config.xmpp.password,
});
xmpp.on('error', console.error);

exports.init = (debug = false) => {
  if(debug) { xmppDebug(xmpp, true); }
  return new Promise((resolve) => {
    if(xmpp.status === 'online') {
      resolve(xmpp);
    }
    else {
      xmpp.once('online', async () => {
        await xmpp.send(xml('presence'));
        resolve(xmpp);
      });
      if(
        xmpp.status === 'offline' ||
        xmpp.status === 'closing' ||
        xmpp.status === 'close' ||
        xmpp.status === 'disconnecting' ||
        xmpp.status === 'disconnect'
      ) {
        console.log('Starting xmpp connection!');
        xmpp.start();
      }
    }
  });
}